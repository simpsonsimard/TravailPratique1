using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;

namespace WindowsApplication3
{

/*******************************************************************************
* Classe:       Formacueille
* Auteurs:      Mathieu Simard, Vincent Poirier, Aminata Gaye
* Date:         27 avril 2006
* Description:  Forme d'accueil lors de l'ouverture du jeu d'�chec
* Modification: Aucune
*******************************************************************************/
    public partial class Formacueille : Form
    { 

/*******************************************************************************
* Formacueille();
* Description: constructeur
* Argument:    aucun
*******************************************************************************/
        public Formacueille()
        {
            InitializeComponent();
        }

/*******************************************************************************
* void button1_Click(object sender, EventArgs e)
* Description: d�marre le serveur et le place en attente
			   initialise la Form1
* Arguments:   object sender, EventArgs e
*******************************************************************************/

        private void button1_Click(object sender, EventArgs e)
        {
            thread = new Thread(new ThreadStart(this.attendre));
            thread.Start();
            
            Form1 f = new Form1();
            f.Show();
			this.Visible = false;
        }


/*******************************************************************************
* void attendre()
* Description: envoie un welcome socket et attend la connection d'un client
* Argument:   Aucun
*******************************************************************************/
        public void attendre()
        {
            welcomeSocket = new TcpListener(IPAddress.Parse("0.0.0.0"), 6799);
            welcomeSocket.Start();

            while (connectionSocket == null)
            {
                connectionSocket = welcomeSocket.AcceptTcpClient();               
            }
            new Thread(new ThreadStart(this.lire_position)).Start();
            
        }

/*******************************************************************************
* void lire_position()
* Description: lors de la r�ception d'un paquet, lis les donn�es et les place dans
			   les variables globales depart et arrivee
* Argument:   Aucun
*******************************************************************************/
					   
        public void lire_position()
        {
            networkStream = connectionSocket.GetStream();
            inFromClient = new StreamReader(networkStream);

            while (true)
            {
                try
                {
                    Form1.depart = Int32.Parse(inFromClient.ReadLine());
                    Form1.arrivee = Int32.Parse(inFromClient.ReadLine());
                }
                catch (Exception) { }
            }   
            
        }

/*******************************************************************************/
        private TcpListener welcomeSocket;
        private StreamReader inFromClient;
        private TcpClient connectionSocket;
        public static NetworkStream networkStream;
        private Thread thread;

        private void Formacueille_FormClosing(object sender, FormClosingEventArgs e)
        {
            welcomeSocket.Stop();
        }
    }
}