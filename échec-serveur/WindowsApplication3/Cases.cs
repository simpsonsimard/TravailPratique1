using System;
using System.Collections.Generic;
using System.Text;

namespace WindowsApplication3
{
   
/*******************************************************************************
* Classe:       Cases
* Auteurs:      Mathieu Simard, Vincent Poirier, Aminata Gaye
* Date:         27 avril 2006
* Description:  Initialisation des cases de l'�chiquier, sous forme de panels
				H�rite de la classe System.Windows.Forms.Panel
* Modification: Aucune
*******************************************************************************/
    public class Cases : System.Windows.Forms.Panel
    {
		/// 
		///position de la case et la pi�ce qui a cette case comme parent
		/// 
        public int position;
        public Piece enfant;

/*******************************************************************************
* Cases();
* Description: constructeur
* Argument:    aucun
*******************************************************************************/
        public Cases()
        {
            position = 0;
            BackColor = System.Drawing.Color.White;
            Height = 75;
            Width = 75;
        }

/*******************************************************************************
* Change_Couleur();
* Description: cr�e une case de couleur noire
* Argument:    aucun
*******************************************************************************/
        public void Change_Couleur()
        {
            BackColor = System.Drawing.Color.Black;
        }

/*******************************************************************************
* Change_Couleur_b();
* Description: cr�e une case de couleur blanche
* Argument:    aucun
*******************************************************************************/
        public void Change_Couleur_b()
        {
            BackColor = System.Drawing.Color.White;
        }
    }
}
