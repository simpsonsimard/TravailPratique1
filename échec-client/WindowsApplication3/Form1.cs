using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;

namespace WindowsApplication3
{   

/*******************************************************************************
* Classe:       Form1
* Auteurs:      Mathieu Simard, Vincent Poirier, Aminata Gaye
* Date:         27 avril 2006
* Description:  Interface du jeu d'�chec
* Modification: Aucune
*******************************************************************************/
    public partial class Form1 : Form
    {
        Formattente f;
        public static Form1 fiche;
        ///
        ///�chiquier
        ///
        public static Cases[] echiq = new Cases[64];
        /// 
        ///garde en m�moire la position d'une pi�ce cliqu�e lors d'un d�placement
        /// 
        int position_piece=-1;
        /// 
        /// Piece, sert de rep�re pour la pi�ce cliqu�e
        /// 
        Piece p;

/*******************************************************************************
* Form1();
* Description: constructeur
* Argument:    aucun
*******************************************************************************/
        public Form1()
        {
            InitializeComponent();
            
        }


/*******************************************************************************
* void Form1_Load(object sender, EventArgs e)
* Description: cr�ation de la Form1
* Arguments:   object sender, EventArgs e
*******************************************************************************/
        private void Form1_Load(object sender, EventArgs e)
        {
            fiche = this;
            timer1.Start();
		///
		///g�n�ration des cases et des pi�ces
		///
            for (int i = 0; i < 64; i++)
            {   
                echiq[i] = new Cases();
                echiq[i].position = i;
                int posy = (i / 8) * 75;
                int posx = (i % 8) * 75;

                echiq[i].Parent = panel1;
                echiq[i].Location = new Point(posx, posy);
                echiq[i].Click += panel1_Click;
                echiq[i].Show();
               
            }

            initialize_couleur_case();
            generer_piece();
        }

/*******************************************************************************
* void button1_Click(object sender, EventArgs e)
* Description: bouton Quitter
* Arguments:   object sender, EventArgs e
*******************************************************************************/
        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

     
/*******************************************************************************
* void panel1_Click(object sender, EventArgs e)
* Description: lorsqu'une case vide est cliqu�e pour l'arriv�e d'un d�placement
* Arguments:   object sender, EventArgs e
*******************************************************************************/   
        private void panel1_Click(object sender, EventArgs e)
        { 
            Cases c = (Cases)sender;
            if (p != null && p.Couleur == "blanc")
            {
                if (position_piece != -1)
                {

                }
                if (p != null)
                {
        /// 
		///Si d�placement valide, on envoie les nouvelles coordonn�es au client,
		///on modifie les variables touch�es et on pr�pare le tour suivant 
		///
                    if (p.mouv_valid(p.position, c.position))
                    {
                        StreamWriter outToServer = new StreamWriter(FormIP.networkStream);
                        outToServer.WriteLine(position_piece.ToString());
                        outToServer.WriteLine(c.position.ToString());
                        outToServer.Flush();

                        p.Parent = c;
                        echiq[position_piece].enfant = null;
                        c.enfant = p;
                        initialize_couleur_case();
                        mon_tour = false;

                        p = null;
                        position_piece = -1;
                        label2.Visible = false;
                        fiche.Enabled = false;
                        f = new Formattente();
                        f.Show();
                    }
                }

            }
        }
   
/*******************************************************************************
* void piece1_Click(object sender, EventArgs e)
* Description: lorsqu'une pi�ce est cliqu�e pour un d�placement ou lorsque
			   l'arriv�e d'un d�placement tombe sur une case occup�e
* Arguments:   object sender, EventArgs e
*******************************************************************************/
        private void piece1_Click(object sender, EventArgs e)
        {
            if (mon_tour == true)
            {
                Piece t = (Piece)sender;
                ///
                ///s'il s'agit de l'arriv�e d'un d�placement et que ce d�placement est valide,
                ///on envoie les nouvelles coordonn�es au client, on modifie les variables 
                ///touch�es et on pr�pare le tour suivant
                ///
                if (position_piece != -1 && p.Parent != t.Parent && t.Couleur != p.Couleur)
                {

                        if (p.mouv_valid(p.position, t.position))
                        {
                            StreamWriter outToServer = new StreamWriter(FormIP.networkStream);
                            outToServer.WriteLine(position_piece.ToString());
                            outToServer.WriteLine(t.position.ToString());
                            outToServer.Flush();

                            p.Parent = t.Parent;
                            echiq[t.position].enfant = echiq[position_piece].enfant;
                            echiq[position_piece].enfant = null;
                            t.Parent = null;

                            ///
                            ///Si la pi�ce �limin�e est le roi, affiche le message de fin de partie
                            ///
                            if (t.est_roi == true)
                            {
                                Form2 fin = new Form2();
                                fin.Show();

                            }
                            else
                            {
                                t = null;
                                position_piece = -1;
                                p = null;
                                mon_tour = false;
                                label2.Visible = false;
                                fiche.Enabled = false;
                                f = new Formattente();
                                f.Show();
                            }
                        }
                        initialize_couleur_case();

                    }


                    else if (position_piece == -1)
                    {
                        if (t.Couleur == "blanc")
                        {
                            p = (Piece)sender;
                            position_piece = p.position;
                            p.Parent.BackColor = System.Drawing.Color.Yellow;
                        }
                    }
                    ///
                    ///Si on re-clique sur la pi�ce d�j� s�lectionn�e, on r�initialise le tout
                    ///
                    else
                    {
                        p = null;
                        position_piece = -1;
                        initialize_couleur_case();
                    }
                }
            
            }
/*******************************************************************************
* void initialize_couleur_case()
* Description: r�-initialise la couleur de la case apr�s le d�placement ou
			   lors de la cr�ation de l'�chiquier
* Argument:    Aucun
*******************************************************************************/
        private void initialize_couleur_case()
        {
            for (int i = 0; i < 64; i++)
            {
                int rangee = 0;
                rangee = i / 8;
                if (rangee % 2 == 0)
                {
                    if (i % 2 == 1)
                    {
                        echiq[i - 1].Change_Couleur_b();
                        echiq[i].Change_Couleur();
                    }
                }
                if (rangee % 2 == 1)
                {
                    if (i % 2 == 0)
                    {
                        echiq[i + 1].Change_Couleur_b();
                        echiq[i].Change_Couleur();
                    }
                }
            }
        }

/*******************************************************************************
* void generer_piece()
* Description: g�n�re les pi�ces lors de la cr�ation de l'�chiquier
* Argument:    Aucun
*******************************************************************************/
        private void generer_piece()
        {
            ///
            /// g�n�ration des pions
            ///

            for (int i = 8; i < 16; i++)
            {
                Pion p = new Pion("noir");
                p.Parent = echiq[i];
                echiq[i].enfant = p;
                p.position = i;
                p.Click += piece1_Click;
                p.Show();
            }

            for (int i = 48; i < 56; i++)
            {
                Pion p = new Pion("blanc");
                p.Parent = echiq[i];
                echiq[i].enfant = p;
                p.position = i;
                p.Click += piece1_Click;
                p.Show();

            }
            ///
            /// g�n�ration des autres pieces
            ///

            Tour t1 = new Tour("noir");
            t1.Parent = echiq[0];
            echiq[0].enfant = t1;
            t1.Show();
            t1.position = 0;
            t1.Click += piece1_Click;
            Tour t2 = new Tour("noir");
            t2.Parent = echiq[7];
            echiq[7].enfant = t2;
            t2.position = 7;
            t2.Show();
            t2.Click += piece1_Click;
            Tour t3 = new Tour("blanc");
            t3.Parent = echiq[56];
            echiq[56].enfant = t3;
            t3.position = 56;
            t3.Show();
            t3.Click += piece1_Click;
            Tour t4 = new Tour("blanc");
            t4.Parent = echiq[63];
            echiq[63].enfant = t4;
            t4.position = 63;
            t4.Show();
            t4.Click += piece1_Click;

            Cavalier c1 = new Cavalier("noir");
            c1.Parent = echiq[1];
            echiq[1].enfant = c1;
            c1.Show();
            c1.position = 1;
            c1.Click += piece1_Click;
            Cavalier c2 = new Cavalier("noir");
            c2.Parent = echiq[6];
            echiq[6].enfant = c2;
            c2.Show();
            c2.position = 6;
            c2.Click += piece1_Click;
            Cavalier c3 = new Cavalier("blanc");
            c3.Parent = echiq[57];
            echiq[57].enfant = c3;
            c3.Show();
            c3.position = 57;
            c3.Click += piece1_Click;
            Cavalier c4 = new Cavalier("blanc");
            c4.Parent = echiq[62];
            echiq[62].enfant = c4;
            c4.Show();
            c4.position = 62;
            c4.Click += piece1_Click;

            Fou f1 = new Fou("noir");
            f1.Parent = echiq[2];
            echiq[2].enfant = f1;
            f1.Click += piece1_Click;
            f1.Show();
            f1.position = 2;
            Fou f2 = new Fou("noir");
            f2.Parent = echiq[5];
            echiq[5].enfant = f2;
            f2.position = 5;
            f2.Click += piece1_Click;
            f2.Show();
            Fou f3 = new Fou("blanc");
            f3.Parent = echiq[58];
            echiq[58].enfant = f3;
            f3.position = 58;
            f3.Click += piece1_Click;
            f3.Show();
            Fou f4 = new Fou("blanc");
            f4.Parent = echiq[61];
            echiq[61].enfant = f4;
            f4.position = 61;
            f4.Click += piece1_Click;
            f4.Show();

            Reine re1 = new Reine("noir");
            re1.Parent = echiq[3];
            echiq[3].enfant = re1;
            re1.Click += piece1_Click;
            re1.position = 3;
            re1.Show();
            Roi ro1 = new Roi("noir");
            ro1.Parent = echiq[4];
            echiq[4].enfant = ro1;
            ro1.position = 4;
            ro1.Click += piece1_Click;
            ro1.Show();

            Reine re2 = new Reine("blanc");
            re2.Parent = echiq[59];
            echiq[59].enfant = re2;
            re2.position = 59;
            re2.Click += piece1_Click;
            re2.Show();
            Roi ro2 = new Roi("blanc");
            ro2.Parent = echiq[60];
            echiq[60].enfant = ro2;
            ro2.position = 60;
            ro2.Click += piece1_Click;
            ro2.Show();
        }

/*******************************************************************************
* void refresh_form(int depart, int arrivee)
* Description: modifie la position et les param�tres des pi�ces lorsqu'un
			   paquet est re�u du client � la suite d'un d�placement
* Arguments:   depart: la position de la pi�ce s'�tant d�plac�e
			   arrivee: la nouvelle position de cette pi�ce
*******************************************************************************/
        public void refresh_form(int depart, int arrivee)
        {
            if (echiq[arrivee].enfant != null)
            {
                echiq[arrivee].enfant.Parent = null;
            }
            echiq[arrivee].enfant = echiq[depart].enfant;
            echiq[depart].enfant.position = arrivee;  
            echiq[depart].enfant.Parent = echiq[arrivee];
            echiq[depart].enfant = null;
            mon_tour = true;
        }

/*******************************************************************************
* void timer1_Tick(object sender, EventArgs e)
* Description: permet de v�rifier r�guli�rement si un paquet � �t� re�u
			   du client suite � un d�placement
* Arguments:   object sender, EventArgs e
*******************************************************************************/

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (depart != -1 && arrivee != -1)
            {
                refresh_form(depart, arrivee);
                depart = -1;
                arrivee = -1;
                fiche.Enabled = true;
                label2.Visible = true;
                f.Close();
            }
        }
		/// 
		///variables globales depart et arrivee re�ues du client 
		///
        public static int depart = -1;
        public static int arrivee = -1;

        /// 
        ///mon_tour d�termine si le tour de jouer est au client ou au serveur 
        ///
        public bool mon_tour = true;
        
                       
    }
}