using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;

namespace WindowsApplication3
{

/*******************************************************************************
* Classe:       FormIP
* Auteurs:      Mathieu Simard, Vincent Poirier, Aminata Gaye
* Date:         27 avril 2006
* Description:  Forme permettant d'entrer l'adresse IP du serveur � rejoindre
* Modification: Aucune
*******************************************************************************/
    public partial class FormIP : Form
    {

/*******************************************************************************
* FormIP();
* Description: constructeur
* Argument:    aucun
*******************************************************************************/
        public FormIP()
        {
            InitializeComponent();
        }

/*******************************************************************************
* void button1_Click(object sender, EventArgs e)
* Description: envoie un paquet au serveur afin de se connecter
			   initialise la Form1
* Arguments:   object sender, EventArgs e
*******************************************************************************/
        private void button1_Click(object sender, EventArgs e)
        {
            clientSocket = new TcpClient(textBox1.Text, 6799);
            networkStream = clientSocket.GetStream();
            new Thread(new ThreadStart(this.lire_position)).Start();

            Form1 f = new Form1();
            f.Show();
            this.Visible = false;
        }

/*******************************************************************************
* void lire_position()
* Description: lors de la r�ception d'un paquet, lis les donn�es et les place dans
			   les variables globales depart et arrivee
* Argument:   Aucun
*******************************************************************************/
        public void lire_position()
        {
            networkStream = clientSocket.GetStream();
            inFromServer = new StreamReader(networkStream);

            while (true)
            {
                try
                {
                    Form1.depart = Int32.Parse(inFromServer.ReadLine());
                    Form1.arrivee = Int32.Parse(inFromServer.ReadLine());
                }
                catch (Exception) { }
            }  

        }
/*******************************************************************************/
        public static TcpClient clientSocket;
        public static NetworkStream networkStream;
        private StreamReader inFromServer;
    }
}