using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace WindowsApplication3
{

/*******************************************************************************
* Classe:       Piece
* Auteurs:      Mathieu Simard, Vincent Poirier, Aminata Gaye
* Date:         27 avril 2006
* Description:  Initialisation des pi�ces
				H�rite de la classe System.Windows.Forms.PictureBox
* Modification: Aucune
*******************************************************************************/
    public class Piece : System.Windows.Forms.PictureBox
    {
		/// 
		/// position et couleur de la pi�ce
		/// 
        public int position;
        public string Couleur;
        /// 
        /// bouger: est utilis� pour le d�placement des pions
        /// est_roi: est utilis� pour d�terminer la fin de la partie
        /// 
        public bool bouger;
        public bool est_roi;

/*******************************************************************************
* Piece();
* Description: constructeur
* Argument:    aucun
*******************************************************************************/
        public Piece()
        {
            bouger = false;
            Height = 75;
            Width = 75;
            position = 0;
            Couleur = "";
        }

/*******************************************************************************
* virtual bool mouv_valid(int depart, int arriver)
* Description: Fonction virtuelle, d�termine si un mouvement est valide.
			   La bonne fonction sera appel�e suivant la nature de la pi�ce 
* Arguments:   depart: position de la pi�ce en d�placement
			   arriver: case d'arriv�e de la pi�ce
* Sortie:	   un bool�en nous indiquant la validit� du d�placement
*******************************************************************************/
        public virtual bool mouv_valid(int depart, int arriver)
        {
                 depart = arriver;
                 return true;
        }
        
    }

/*******************************************************************************
* Classe:       Pion
* Auteurs:      Mathieu Simard, Vincent Poirier, Aminata Gaye
* Date:         27 avril 2006
* Description:  Initialisation du pion
				H�rite de la classe Piece
* Modification: Aucune
*******************************************************************************/
    public class Pion : Piece
    {

/*******************************************************************************
* Pion(string color);
* Description: constructeur
* Argument:    color : la couleur de la pi�ce cr��e
*******************************************************************************/
        public Pion(string color)
        {
            bouger = false;
            Couleur = color;
            est_roi = false;
            if (color == "noir")
            {
                BackgroundImage = Image.FromFile("..\\..\\Resources\\pionnoir.gif");

            }
            else
            {
                BackgroundImage = Image.FromFile("..\\..\\Resources\\pionblanc.gif");
            }
        }

/*******************************************************************************
* override bool mouv_valid(int depart, int arriver)
* Description: d�termine si le mouvement de la pi�ce est valide
* Arguments:   depart: position de la pi�ce en d�placement
			   arriver: case d'arriv�e de la pi�ce
* Sortie:	   un bool�en nous indiquant la validit� du d�placement
*******************************************************************************/
        public override bool mouv_valid(int depart, int arriver)
        {
            if (Couleur == "blanc")
            {
		///
		///Si le pion n'a pas boug�, il peut avancer de 2 cases
		///
                if (bouger == false && 0 == (arriver - depart) % 8 && -2 == (arriver - depart) / 8)
                {
                    if (Form1.echiq[depart - 8].enfant == null)
                    {
                        bouger = true;
                        position = arriver;
                        return true;
                    }
                    else return false;
                }
		///
		///Sinon, il avance d'une case seulement
		///
                else if (0 == (arriver - depart) % 8 && -1 == (arriver - depart) / 8)
                {
                    if (Form1.echiq[depart - 8].enfant == null)
                    {
                        bouger = true;
                        position = arriver;
                        return true;
                    }
                    else return false;
                }

		///
		///Attaque du pion en diagonale
		///
                else if (arriver - depart == -9 || arriver - depart == -7)
                {
            
                        if (Form1.echiq[arriver].enfant != null)
                        {
                            bouger = true;
                            position = arriver;
                            return true;
                        }
                        else return false;
                   

                }

                else return false;
            }
            else return false;
        }
    }

/*******************************************************************************
* Classe:       Tour
* Auteurs:      Mathieu Simard, Vincent Poirier, Aminata Gaye
* Date:         27 avril 2006
* Description:  Initialisation de la tour
				H�rite de la classe Piece
* Modification: Aucune
*******************************************************************************/


    public class Tour : Piece
    {

/*******************************************************************************
* Tour(string color);
* Description: constructeur
* Argument:    color : la couleur de la pi�ce cr��e
*******************************************************************************/
        public Tour(string color)
        {
            Couleur = color;
            est_roi = false;
            if (color == "noir")
            {
                BackgroundImage = Image.FromFile("..\\..\\Resources\\tour_noire.gif");

            }
            else
            {
                BackgroundImage = Image.FromFile("..\\..\\Resources\\tour_blanche.gif");
            }

        }

/*******************************************************************************
* override bool mouv_valid(int depart, int arriver)
* Description: d�termine si le mouvement de la pi�ce est valide
* Arguments:   depart: position de la pi�ce en d�placement
			   arriver: case d'arriv�e de la pi�ce
* Sortie:	   un bool�en nous indiquant la validit� du d�placement
*******************************************************************************/
        public override bool mouv_valid(int depart, int arriver)
        {
		///
		///Mouvement vertical
		///
            if (arriver % 8 == depart % 8)
            {
		///
		///S'assure qu'il n'y a pas de pi�ce entre le d�part et l'arriv�e
		///lors d'un d�placement vers le haut
		///
                if (arriver - depart < 0)
                {
                    for (int i = depart - 8; i > arriver; i -= 8)
                    {
                        if (Form1.echiq[i].enfant != null)
                        {
                            return false;
                        }
                    }
                    bouger = true;
                    position = arriver;
                    return true;

                }
                else
                {
		///
		///S'assure qu'il n'y a pas de pi�ce entre le d�part et l'arriv�e
		///lors d'un d�placement vers le bas
		///
                    for (int i = depart + 8; i < arriver; i += 8)
                    {
                        if (Form1.echiq[i].enfant != null)
                        {
                            return false;
                        }
                    } 
                    bouger = true;
                    position = arriver;
                    return true;
                
                }
            }
		///
		///Mouvement horizontal
		///
            else if (arriver / 8 == depart / 8)
            {
		///
		///S'assure qu'il n'y a pas de pi�ce entre le d�part et l'arriv�e
		///lors d'un d�placement vers la gauche
		///
                if (arriver - depart < 0)
                {
                    for (int i = depart - 1; i > arriver; i -= 1)
                    {
                        if (Form1.echiq[i].enfant != null)
                        {
                            return false;
                        }
                    }
                    bouger = true;
                    position = arriver;
                    return true;

                }
                else
                {
		///
		///S'assure qu'il n'y a pas de pi�ce entre le d�part et l'arriv�e
		///lors d'un d�placement vers la droite
		///
                    for (int i = depart + 1; i < arriver; i += 1)
                    {
                        if (Form1.echiq[i].enfant != null)
                        {
                            return false;
                        }
                    }
                    bouger = true;
                    position = arriver;
                    return true;

                }
            }
            else return false;
        }

    }

/*******************************************************************************
* Classe:       Roi
* Auteurs:      Mathieu Simard, Vincent Poirier, Aminata Gaye
* Date:         27 avril 2006
* Description:  Initialisation du roi
				H�rite de la classe Piece
* Modification: Aucune
*******************************************************************************/

    public class Roi : Piece
    {

/*******************************************************************************
* Roi(string color);
* Description: constructeur
* Argument:    color : la couleur de la pi�ce cr��e
*******************************************************************************/
        public Roi(string color)
        {
            Couleur = color;
            est_roi = true;

            if (color == "noir")
            {
                BackgroundImage = Image.FromFile("..\\..\\Resources\\hank_noir.gif");

            }
            else
            {
                BackgroundImage = Image.FromFile("..\\..\\Resources\\hank_blanc.gif");
            }

        }

/*******************************************************************************
* override bool mouv_valid(int depart, int arriver)
* Description: d�termine si le mouvement de la pi�ce est valide
* Arguments:   depart: position de la pi�ce en d�placement
			   arriver: case d'arriv�e de la pi�ce
* Sortie:	   un bool�en nous indiquant la validit� du d�placement
*******************************************************************************/
        public override bool mouv_valid(int depart, int arriver)
        {int i= arriver-depart;
        ///
		///d�placement d'une case � la fois
		/// 
            if ((i==1) || (i==-1)|| (i==7)|| (i==-7)||(i==8)||(i==-8)||(i==9)||(i==-9))
            {
                bouger = false;
                position = arriver;
                return true;
            }
            else return false;
        }
    }
/*******************************************************************************
* Classe:       Reine
* Auteurs:      Mathieu Simard, Vincent Poirier, Aminata Gaye
* Date:         27 avril 2006
* Description:  Initialisation de la reine
				H�rite de la classe Piece
* Modification: Aucune
*******************************************************************************/

   public class Reine : Piece
    {
/*******************************************************************************
* Reine(string color);
* Description: constructeur
* Argument:    color : la couleur de la pi�ce cr��e
*******************************************************************************/
        public Reine(string color)
        {
            Couleur = color;
            est_roi = false;

            if (color == "noir")
            {
                BackgroundImage = Image.FromFile("..\\..\\Resources\\reine_noire.bmp");

            }
            else
            {
                BackgroundImage = Image.FromFile("..\\..\\Resources\\reine_blanche.bmp");
            }

        }

/*******************************************************************************
* override bool mouv_valid(int depart, int arriver)
* Description: d�termine si le mouvement de la pi�ce est valide
* Arguments:   depart: position de la pi�ce en d�placement
			   arriver: case d'arriv�e de la pi�ce
* Sortie:	   un bool�en nous indiquant la validit� du d�placement
*******************************************************************************/
        public override bool mouv_valid(int depart, int arriver)
        {
		///
		///La reine peut emprunter soit le mouvement de la tour...
		///
            Tour t = new Tour("blanc");
            if (t.mouv_valid(depart, arriver))
            {
                position = arriver;
                return true;
            }else
            {
		///
		///... ou le mouvement du fou
		///
                Fou f = new Fou("blanc");
                if (f.mouv_valid(depart, arriver))
                {
                    position = arriver;
                    return true;
                }

            } return false;
        }
    }


/*******************************************************************************
* Classe:       Fou
* Auteurs:      Mathieu Simard, Vincent Poirier, Aminata Gaye
* Date:         27 avril 2006
* Description:  Initialisation du fou
				H�rite de la classe Piece
* Modification: Aucune
*******************************************************************************/
    public class Fou : Piece
    {

/*******************************************************************************
* Fou(string color);
* Description: constructeur
* Argument:    color : la couleur de la pi�ce cr��e
*******************************************************************************/
        public Fou(string color)
        {
            Couleur = color;
            est_roi = false;

            if (color == "noir")
            {
                BackgroundImage = Image.FromFile("..\\..\\Resources\\jester_noir.gif");

            }
            else
            {
                BackgroundImage = Image.FromFile("..\\..\\Resources\\foublanc.gif");
            }

        }

/*******************************************************************************
* override bool mouv_valid(int depart, int arriver)
* Description: d�termine si le mouvement de la pi�ce est valide
* Arguments:   depart: position de la pi�ce en d�placement
			   arriver: case d'arriv�e de la pi�ce
* Sortie:	   un bool�en nous indiquant la validit� du d�placement
*******************************************************************************/
        public override bool mouv_valid(int depart, int arriver)
        {
            int deplace = (arriver / 8) - (depart / 8);
		///
		///d�placement vers le haut
		///
            if (deplace < 0)
            {
		///
		///s'assure qu'il n'y a pas de pi�ce entre le d�part et l'arriv�e lors
		///d'un d�placement vers la gauche
		///
                if (arriver == (depart + 8 * deplace - deplace))
                {
                    for (int i = -1; i > deplace; i--)
                    {
                        if (Form1.echiq[depart + 8 * i - i].enfant != null)
                        {
                            return false;
                        }

                    }
                    position = arriver;
                    return true;

                }
		///
		///s'assure qu'il n'y a pas de pi�ce entre le d�part et l'arriv�e lors
		///d'un d�placement vers la droite
		///
                else if (arriver == (depart + 8 * deplace + deplace))
                {
                    for (int i = -1; i > deplace; i--)
                    {
                        if (Form1.echiq[depart + 8 * i + i].enfant != null)
                        {
                            return false;
                        }

                    }
                    position = arriver;
                    return true;

                }
            }
            else
            {
		///
		///d�placement vers le bas
		///
		///
		///s'assure qu'il n'y a pas de pi�ce entre le d�part et l'arriv�e lors
		///d'un d�placement vers la gauche
		///

                if (arriver == (depart + 8 * deplace + deplace))
                {
                    for (int i = 1; i < deplace; i++)
                    {
                        if (Form1.echiq[depart + 8 * i + i].enfant != null)
                        {
                            return false;
                        }

                    }
                    position = arriver;
                    return true;

                }
		///
		///s'assure qu'il n'y a pas de pi�ce entre le d�part et l'arriv�e lors
		///d'un d�placement vers la droite
		///
                else if (arriver == (depart + 8 * deplace - deplace))
                {
                    for (int i = 1; i < deplace; i++)
                    {
                        if (Form1.echiq[depart + 8 * i - i].enfant != null)
                        {
                            return false;
                        }

                    }
                    position = arriver;
                    return true;

                }
            }
		///
		///si aucune des conditions n'a pu retourner true, 
		///le d�placement n'est pas valide
		///
            return false;
        }
    }


/*******************************************************************************
* Classe:       Cavalier
* Auteurs:      Mathieu Simard, Vincent Poirier, Aminata Gaye
* Date:         27 avril 2006
* Description:  Initialisation du cavalier
				H�rite de la classe Piece
* Modification: Aucune
*******************************************************************************/
    public class Cavalier : Piece
    {

/*******************************************************************************
* Cavalier(string color);
* Description: constructeur
* Argument:    color : la couleur de la pi�ce cr��e
*******************************************************************************/
        public Cavalier(string color)
        {
            Couleur = color;
            est_roi = false;

            if (color == "noir")
            {
                BackgroundImage = Image.FromFile("..\\..\\Resources\\chevaux_noir.gif");
            }
            else
            {
                BackgroundImage = Image.FromFile("..\\..\\Resources\\chevauxblanc.gif");
            }

        }

/*******************************************************************************
* override bool mouv_valid(int depart, int arriver)
* Description: d�termine si le mouvement de la pi�ce est valide
* Arguments:   depart: position de la pi�ce en d�placement
			   arriver: case d'arriv�e de la pi�ce
* Sortie:	   un bool�en nous indiquant la validit� du d�placement
*******************************************************************************/
        public override bool mouv_valid(int depart, int arriver)
        {
           int i= depart - arriver;
		///
		///d�placement en "L" permis seulement
		///
            if (i==-17|| i==-15 || i==-10 ||i==-6||i==6||i==10||i==15||i==17)
            {   
                position = arriver;
                return true;  
            }
            else return false;
        }
    }
}
