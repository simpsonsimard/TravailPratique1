using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WindowsApplication3
{

/*******************************************************************************
* Classe:       Form2
* Auteurs:      Mathieu Simard, Vincent Poirier, Aminata Gaye
* Date:         27 avril 2006
* Description:  Forme de fin de partie
* Modification: Aucune
*******************************************************************************/
    public partial class Form2 : Form
    {

/*******************************************************************************
* Form2();
* Description: constructeur
* Argument:    aucun
*******************************************************************************/
        public Form2()
        {
            InitializeComponent();
        }

/*******************************************************************************
* void button1_Click(object sender, EventArgs e)
* Description: bouton Quitter
* Arguments:   object sender, EventArgs e
*******************************************************************************/
        private void button1_Click(object sender, EventArgs e)
        {
            Form1.fiche.Close();

            Application.Exit();
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }
    }
}