using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WindowsApplication3
{

/*******************************************************************************
* Classe:       Formacueille
* Auteurs:      Mathieu Simard, Vincent Poirier, Aminata Gaye
* Date:         27 avril 2006
* Description:  Forme d'accueil lors de l'ouverture du jeu d'�chec
* Modification: Aucune
*******************************************************************************/
    public partial class Formacueille : Form
    {

/*******************************************************************************
* Formacueille();
* Description: constructeur
* Argument:    aucun
*******************************************************************************/
        public Formacueille()
        {
            InitializeComponent();
        }

/*******************************************************************************
* void button2_Click(object sender, EventArgs e)
* Description: ouvre la forme FormIP
* Arguments:   object sender, EventArgs e
*******************************************************************************/

        private void button2_Click(object sender, EventArgs e)
        {  
            FormIP f1 = new FormIP();
            f1.Show();
            this.Visible = false;  
        }

        private void Formacueille_Load(object sender, EventArgs e)
        {

        }

    }
}